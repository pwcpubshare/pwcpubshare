//
//  HomeItemCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/8.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit
import MobilePlayer

class HomeItemCell: UITableViewCell {
    var callBack: (() ->())?
    
    @IBOutlet weak var avator: JZImageView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var people: UILabel!
    
    @IBOutlet weak var favorite: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var name: UILabel!
    @IBAction func playAction(sender: AnyObject) {
        callBack?()
    }
    
    @IBOutlet weak var screen: UIImageView!
    var titleSections = ["Senior Associate",
                         "Senior Developer",
                         "Developer",
                         "Senior Managment",
                         "Intern",
                         "Managment"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        people.text = String(arc4random() % 100)
        favorite.text = String(arc4random() % 100)
        title.text = titleSections[Int(arc4random() % 6)]
        time.text = String(arc4random() % 60) + "minutes ago"
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
