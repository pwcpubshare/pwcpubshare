//
//  SquareCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/21.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class SquareCell: UICollectionViewCell

{
    var type: String = "" {
        didSet {
            typeLabel.text = type
        }
    }
    
    @IBOutlet weak var typeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
