//
//  RoundTableTopicCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/27.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class RoundTableTopicCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    var title = ""{
        didSet {
            titleLabel.text = title
        }
    }
    @IBOutlet weak var join: UIButton!
    @IBOutlet weak var days: UILabel!
    
    @IBOutlet weak var people: UILabel!
    
    @IBOutlet weak var favorite: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        join.hidden = true
        days.text = String(arc4random() % 30 + 1) + "days left"
        people.text = String(arc4random() % 100)
        favorite.text = String(arc4random() % 100)
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
