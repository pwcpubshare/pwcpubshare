//
//  RoundTableDetailViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/15.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class RoundTableDetailViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        configureTableView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Plus"), style: .Plain, target: self, action: #selector(presentActionSheet))
    }

    override func configureTableView() {
        tableView.estimatedRowHeight = 44
        tableView.separatorStyle = .None
        tableView.registerNibWithCell(PostDescriptionCell)
        tableView.registerNibWithCell(PostSketchCell)
        tableView.registerNibWithCell(PostDiscussionCell)
    }
    
    dynamic private func presentActionSheet(sourceView: UIView) {
        let source: [(String, Selector)] = [("Set Up Meeting", #selector(setupMeeting)), ("Post Topic", #selector(postTopic))]
        configActionsheet(source, sourceView: sourceView, target: self)
    }
    
    func postTopic() {
        
    }
    
    func setupMeeting() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print(tableView)
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return section == 2 ? 3 : 1
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueCell(PostDescriptionCell)
            return cell
        case 1:
            let cell = tableView.dequeueCell(PostSketchCell)
            return cell
        default:
           let cell = tableView.dequeueCell(PostDiscussionCell)
            let number = indexPath.row + 4
           cell.name.text = nameSections[indexPath.row]
            cell.jzImageView.image = UIImage(named: "Oval 46 Copy " + String(number))
            return cell
        }
        
        // Configure the cell...

    }
    
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = NSBundle.mainBundle().loadNibNamed("PostFooterCell", owner: nil, options: nil).first as? PostFooterCell
        switch section {
        case 0:
            return nil
        case 1:
            header!.title = "Members"
        default:
            header!.title = "Discussions"
        }
        return header
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 33
    }
    

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 2 {
            let discussionDetailVC = DiscussionDetailViewController()
            navigationController?.pushViewController(discussionDetailVC, animated: true)
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
