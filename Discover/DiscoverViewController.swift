//
//  DiscoverViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/15.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class DiscoverViewController: UIViewController {

    @IBOutlet weak var container: UIScrollView!
    @IBOutlet weak var topicButton: UIButton!
    private var previousSelectedButton: UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureTableView()
        navigationItem.title = "Discover"
        previousSelectedButton = topicButton
        
        container.contentSize = CGSize(width: container.frame.size.width * 3, height: container.frame.size.height)
        container.pagingEnabled = true
        container.scrollEnabled = false
        addSubViewController(RoundTableTopicViewController(), TopicTableViewController(), RoundTableListViewController())

    }

    func addSubViewController(childVCs: UIViewController...) {
        for (index, childVC) in childVCs.enumerate() {
            addChildViewController(childVC)
            childVC.view.frame = container.bounds
            childVC.view.frame.origin.x = CGFloat(index) * Screen_Width
            container.addSubview(childVC.view)
        }
    }
    
    @IBAction func typeSwitch(sender: UIButton) {
        
        previousSelectedButton?.setTitleColor(UIColor.blackColor(), forState: .Normal)
        previousSelectedButton?.titleLabel?.font = FONT_MEDIUM14
        previousSelectedButton = sender
        CATransaction.setDisableActions(false)
        UIView.animateWithDuration(0.25) { 
            sender.titleLabel?.font = FONT_BOLD16
            sender.setTitleColor(CustomRedColor, forState: .Normal)
            self.container.contentOffset.x = CGFloat(sender.tag) * Screen_Width
        }

    }
    
//    override func configureTableView() {
//        super.configureTableView()
//        tableView.registerNibWithCell(HomeItemCell)
//        tableView.estimatedRowHeight = 100
//        
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


//extension DiscoverViewController: UITableViewDataSource {
//    
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return 1
//    }
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 0
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueCell(HomeItemCell)
//        return cell
//    }
//}

