//
//  TagsTableViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/24.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

enum Topics:Int {
    case tags = 0
    case toFriend
}

class TagsTableViewController: UITableViewController {

    var tags = ["Front end development",
                "CSS",
                "HTML",
                "JavaScript",
                "Techniques"]
    var type: Topics = .tags{
        didSet {
            if type == .tags {
                tags = ["Front end development",
                        "CSS",
                        "HTML",
                        "JavaScript",
                        "Techniques"]
            } else {
                tags = ["Beverly Crawford",
                        "Janet Stone",
                        "Jennifer Ward",
                        "Kevin Murphy",
                        "Randy Weber",
                        "Victoria Patel"]
            }
        }
    }
    var tagsState:[Bool] = []
    var stringStorage = ""
    var callBack: ((str: String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        for  _ in 0..<tags.count {
            tagsState.append(false)
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        configureTableView()
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: Screen_Width, height: 40))
        navigationItem.titleView = searchBar
    }

    override func configureTableView() {
        tableView.estimatedRowHeight = 44
        tableView.keyboardDismissMode = .OnDrag
        tableView.separatorStyle = .None
        tableView.registerNibWithCell(FriendNameCell)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        var str = ""
        for (index, tag) in tagsState.enumerate() {
            str = tag ? str + (tags[index] + ", ") : str
        }
        stringStorage = str
        callBack?(str: stringStorage)
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(FriendNameCell)
        cell.label.text = tags[indexPath.row]
        configCell(indexPath, cell: cell)
        return cell
    }
    func configCell(indexPath: NSIndexPath, cell: FriendNameCell?) {
        cell?.accessoryType = tagsState[indexPath.row] ? .Checkmark : .None
        cell?.label.textColor = tagsState[indexPath.row] ? CustomRedColor : CustomGrayColor
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = NSBundle.mainBundle().loadNibNamed("PostHeaderCell", owner: self, options: nil).first as? PostHeaderCell
        if type == .tags {
            cell?.label.text = "Suggested tags"
        } else {
            cell?.label.text = "Suggested Experts"

        }
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tagsState[indexPath.row] = !tagsState[indexPath.row]
        let cell = tableView.cellForRowAtIndexPath(indexPath) as? FriendNameCell
        configCell(indexPath, cell: cell)
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
