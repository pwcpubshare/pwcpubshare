//
//  WebinarViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/13.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit
import MobilePlayer
class WebinarViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        addBackgroundImage()
        
        configureRightBar("Next")
        // Do any additional setup after loading the view.
    }

    override func rightBarAction() {
        
        let video = VideoController()
        navigationController?.pushViewController(video, animated: true)
    }
    
    func addBackgroundImage() {
        let  imageView = UIImageView(frame: view.frame)
        imageView.backgroundColor = UIColor.whiteColor()
        imageView.image = UIImage(named: "page-1-walkthrough-2")
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(play))
        imageView.userInteractionEnabled = true
        imageView.addGestureRecognizer(tapGesture)
        view.addSubview(imageView)
    }
    
    func play() {
        let bundle = NSBundle.mainBundle().pathForResource("video", ofType: "mp4")
        let url = NSURL(fileURLWithPath: bundle!)
        let mobileplayer = MobilePlayerViewController(contentURL: url)
        mobileplayer.activityItems = [url]
        mobileplayer.title = "Applied Data Science with Python"
        self.presentMoviePlayerViewControllerAnimated(mobileplayer)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

