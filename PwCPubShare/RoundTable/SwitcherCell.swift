//
//  SwitcherCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/7.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class SwitcherCell: UITableViewCell {

    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var label: UILabel!
    var callBack: ( (on: Bool) -> Void)?
    var title: String = "" {
        didSet {
            label.text = title
        }
    }
    @IBAction func stateChanged(sender: UISwitch) {
        callBack?(on: sender.on)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
