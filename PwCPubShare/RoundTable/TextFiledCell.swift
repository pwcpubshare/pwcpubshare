//
//  TextFiledCell.swift
//  
//
//  Created by Jun Zhang on 2017/2/8.
//
//

import UIKit

class TextFiledCell: UITableViewCell {

    @IBOutlet weak var textfiled: UITextField!
    var title: String = "" {
        didSet {
            textfiled.placeholder = title
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension TextFiledCell: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textfiled.resignFirstResponder()
        return true
    }
}