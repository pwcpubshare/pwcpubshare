//
//  AddViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/7.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func configureTableView() {
        
    }
    
    func rightBarAction() {
        
    }
    
    func configureRightBar(title: String) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: title, style: .Plain, target: self, action: #selector(UIViewController.rightBarAction))
    }
}

class AddViewController: UITableViewController {

    var imageNames:[String] = ["Topics icon", "Webinar icon", "Round Table icon"]
    var types:[String] = ["Topics", "Webinar", "RoundTable"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        
        configureTableView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: #selector(UIViewController.cancelButtonAction))
//        let CG_PI_2 = CGFloat(M_PI_4)
//        view.layer.transform = CATransform3DMakeRotation(CG_PI_2, 0, 0, 1)
    }

    override func configureTableView() {
        tableView.registerNibWithCell(TypeCell)
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .None
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(TypeCell)
        cell.type.image = UIImage(named: imageNames[indexPath.row])
        cell.typeLabel.text = types[indexPath.row]
        return cell
    }
 

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var vc:UIViewController!
        switch indexPath.row {
        case 0:
            vc = TopicFirstPageViewController()
        case 1:
            vc = WebinarViewController()
        case 2:
            vc = RoundtableViewController()
        default:
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
