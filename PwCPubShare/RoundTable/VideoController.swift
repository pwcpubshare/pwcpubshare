//
//  VideoController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/13.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class VideoController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addBackgroundImage()
        configureRightBar("Next")
    }

    
    
    func addBackgroundImage() {
        let  imageView = UIImageView(frame: view.frame)
        imageView.backgroundColor = UIColor.whiteColor()
        imageView.image = UIImage(named: "page-1-webninar-step3")
        view.addSubview(imageView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
