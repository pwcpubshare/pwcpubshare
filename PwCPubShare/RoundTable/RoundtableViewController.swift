//
//  RoundtableViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/8.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit


func configActionsheet(info: [(String, Selector)], sourceView: UIView, target: UIViewController)  {
    let alertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
    for (title , action) in info {
        let actions = UIAlertAction(title: title, style: .Destructive, handler: { (actions) in
            if target.respondsToSelector(action) {
                target.performSelector(action)
            }
        })
        alertController.addAction(actions)
    }
    
    dispatch_async(dispatch_get_main_queue()) {
            let cancelActions = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            alertController.addAction(cancelActions)
        target.presentViewController(alertController, animated: true, completion: nil)
    }
}

extension UIViewController {
    
    func configRightBar(config: (name: String, method: Selector)) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: config.name, style: .Plain, target: self, action: config.method)
    }
}

class RoundtableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configRightBar(("Next", #selector(nextAction)))
//        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: #selector(nextAction))
    }
    
    
    
    override func configureTableView() {
        tableView.registerNibWithCell(TextFiledCell)
        tableView.registerNibWithCell(SwitcherCell)
        tableView.keyboardDismissMode = .OnDrag
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .None
        let header = UIView(frame: CGRect(x: 0, y: 0, width: Screen_Width, height: 20))
        header.backgroundColor = UIColor.whiteColor()
        tableView.tableHeaderView = header
    }
    
    func nextAction() {
     let inviteFriendVC = RoundTableInviteFriendsController()
        navigationController?.pushViewController(inviteFriendVC, animated: true)
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        return configureCell(indexPath)
    }

    func configureCell(indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 1:
            let cell = tableView.dequeueCell(SwitcherCell)
            
            cell.title = "Keep it private"
            return cell

        case 2:
            let cell = tableView.dequeueCell(SwitcherCell)
            cell.title = "Keep data saved"
            return cell
        default:
            let cell = tableView.dequeueCell(TextFiledCell)
            return cell
        }
    }
    
//    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 20
//    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
