//
//  TypeCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/13.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class TypeCell: UITableViewCell {

    @IBOutlet weak var type: UIImageView!
    
    @IBOutlet weak var typeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
