//
//  TopicGuideViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/7.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class TopicGuideViewController: UIViewController {
    @IBOutlet weak var collectionviewHeight: NSLayoutConstraint!

    @IBOutlet weak var collectionView: UICollectionView!
    
    var imageNames: [String] = ["AdobeIcon", "GuidewireIcon", "MicrosoftIcon", "MobilityIcon", "OracleIcon", "SalesforceIcon", "SAPIcon", "TestingIcon", "UXIcon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        congfigCollectionView()
        // Do any additional setup after loading the view.
    }
    
    private func congfigCollectionView() {
        let width = UIScreen.mainScreen().bounds.width - 30
        collectionviewHeight.constant = width
        collectionView.registerNibWithCell(ImageCollectionCell)
    }
    
    
    
    @IBAction func updateRootViewController(sender: AnyObject) {
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let tabarVC = sb.instantiateViewControllerWithIdentifier("RAM")
        UIView.transitionFromView(view, toView: tabarVC.view, duration: 0.3, options: .TransitionFlipFromBottom) { (bool) in
            UIApplication.sharedApplication().keyWindow?.rootViewController = tabarVC
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension TopicGuideViewController: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(ImageCollectionCell.self, indexPath: indexPath)
        cell.imageName = imageNames[indexPath.item]
        return cell
    }
    
}

extension TopicGuideViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 15
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = (UIScreen.mainScreen().bounds.width - 60) / 3
        return CGSize(width: width, height: width)
    }
}

