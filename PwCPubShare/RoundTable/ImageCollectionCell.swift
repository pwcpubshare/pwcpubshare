//
//  ImageCollectionCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/7.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageButton: UIButton!
    
    var imageName: String = "" {
        didSet {
            guard let image = UIImage(named: imageName) else {
                return
            }
//            imageButton.setImage(image, forState: .Normal)
            imageButton.setBackgroundImage(image, forState: .Normal)
        }
    }
    dynamic var showhook = false
    dynamic var shapeLayer: CAShapeLayer?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    

    @IBAction func clickAction(sender: UIButton) {
            showhook = !showhook
           sender.selected = !sender.selected
//        let center = CGPoint(x: CGRectGetMaxX(sender.bounds) * 0.75, y: CGRectGetMaxY(sender.bounds) * 0.8)
        if showhook {
//            drawSuccessfulMarkAnimation(30, center: sender.center)
            UIView.transitionWithView(sender, duration: 0.5, options: .TransitionFlipFromLeft, animations: {
//                sender.backgroundColor = UIColor.blackColor()
//                sender.alpha = 0.6
//                sender.layer.cornerRadius = sender.bounds.width * 0.5

                }, completion: nil)
//            UIView.animateWithDuration(0.5, animations: {
//                sender.transform = CGAffineTransformMakeScale(0.6, 0.6)
//                
//            })
        } else {
            shapeLayer?.strokeColor = UIColor.clearColor().CGColor
            UIView.transitionWithView(sender, duration: 0.5, options: .TransitionFlipFromRight, animations: {
//                sender.backgroundColor = UIColor.whiteColor()
//                sender.alpha = 1.0
//                sender.layer.cornerRadius = 0
                }, completion: nil)

        }
        
    }
    func drawSuccessfulMarkAnimation(radius: CGFloat, center: CGPoint) {
        //Create Hook Path
        let offset = radius/2
        let linePath = UIBezierPath()
        linePath.lineCapStyle = CGLineCap.Round
        linePath.lineJoinStyle = CGLineJoin.Round
        linePath.moveToPoint(CGPointMake(center.x - radius + 6, center.y))
        linePath.addLineToPoint(CGPointMake(center.x - offset + 6, center.y + (offset)))
        linePath.addLineToPoint(CGPointMake(center.x + offset + 3, center.y - offset + 2))

        let shapeLayer = CAShapeLayer()
        shapeLayer.path = linePath.CGPath
        shapeLayer.strokeColor = UIColor.whiteColor().CGColor
        shapeLayer.lineWidth = 5
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        imageButton.layer.addSublayer(shapeLayer)
        self.shapeLayer = shapeLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0.0
        animation.toValue = 1.0
        animation.duration = 0.5
        animation.beginTime = 0.0
        animation.removedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        shapeLayer.addAnimation(animation, forKey: "hook")
    }

    
}
