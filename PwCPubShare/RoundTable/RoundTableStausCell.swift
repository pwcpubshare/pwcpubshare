//
//  RoundTableStausCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/9.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class RoundTableStausCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    var title = ""{
        didSet {
            titleLabel.text = title
        }
    }
    @IBOutlet weak var join: UIButton!
    @IBOutlet weak var days: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        days.text = String(arc4random() % 30 + 1) + "days left"
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func joinAction(sender: AnyObject) {
    }
}
