//
//  TopicFirstPageViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/24.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

func -(lhs: String, rhs: String) -> String{
    if lhs.characters.count < rhs.characters.count {
        assert(true, "empty message")
        return lhs
    }
    let number = lhs.characters.count - rhs.characters.count
    let index = lhs.startIndex.advancedBy(number)
    let rangeStr = lhs.substringToIndex(index)
    let extraStr = lhs.substringFromIndex(index)
    return extraStr == rhs ? rangeStr : lhs
}
    

class TopicFirstPageViewController: RoundtableViewController {

    var requesetHelp = false {
        didSet {
            if requesetHelp != oldValue {
                tableView.reloadData()
            }
        }
    }
    
    var emptyTags = ""
    var emptyFriends = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func nextAction() {
        let secondVC = TopicSecondViewController()
        navigationController?.pushViewController(secondVC, animated: true)
    }
    

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return requesetHelp ? 4 : 3
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 2:
            let cell = tableView.dequeueCell(SwitcherCell)
            cell.callBack = {
                [weak self] on in
                self?.requesetHelp = on
            }
            cell.title = "You would like to ask"
            return cell
        default:
            let cell = tableView.dequeueCell(TextFiledCell)
            cell.textfiled.enabled = indexPath.row == 0
            if indexPath.row == 0 {
                cell.title = "Topic title?"
            } else if indexPath.row == 1 {
                cell.title = "Tags"
                cell.textfiled.text = emptyTags
            } else {
                cell.title = "To"
                cell.textfiled.text = emptyFriends
            }
            return cell
        }
    }

    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let tagVC = TagsTableViewController()
        tagVC.stringStorage = emptyTags
        if indexPath.row == 3 {
            tagVC.type = .toFriend
        }
        tagVC.callBack = {
            [weak self] str in
            if tagVC.type == .toFriend {
                self?.emptyFriends = str - ","
            } else {
                self?.emptyTags = str - ","
            }
        }
        navigationController?.pushViewController(tagVC, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
