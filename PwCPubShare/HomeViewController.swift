//
//  UserViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 17/1/7.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import MobilePlayer
import AVKit
let FONT_NORMAL12 = UIFont(name: "Helvetica Neue", size: 12)
let FONT_NORMAL13 = UIFont(name: "Helvetica Neue", size: 13)
let FONT_NORMAL14 = UIFont(name: "Helvetica Neue", size: 14)
let FONT_NORMAL15 = UIFont(name: "Helvetica Neue", size: 15)
let FONT_NORMAL16 = UIFont(name: "Helvetica Neue", size: 16)
let FONT_NORMAL17 = UIFont(name: "Helvetica Neue", size: 17)
let FONT_MEDIUM22 = UIFont(name: "HelveticaNeue-Medium", size: 22)
let FONT_MEDIUM18 = UIFont(name: "HelveticaNeue-Medium", size: 18)
let FONT_MEDIUM14 = UIFont(name: "HelveticaNeue-Medium", size: 14)

let FONT_BOLD24 = UIFont(name: "HelveticaNeue-Bold", size: 24)
let FONT_BOLD16 = UIFont(name: "HelveticaNeue-Bold", size: 16)
let nameSections = ["Gregory Morrison", "Walter Garcia", "John Wilson"]

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var animatedItem: RAMAnimatedTabBarItem!
    
//    @IBOutlet weak var shimmerView: FBShimmeringView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Home"
        navigationController?.delegate = self
        tabBarController?.delegate = self
        configureTableView()
        
     
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Plus"), style: .Plain, target: self, action: #selector(presentToRoundTableViewController))
        
//         let contentView = UILabel(frame: shimmerView.bounds)
//        contentView.backgroundColor = UIColor.greenColor()
//        contentView.textColor = UIColor.whiteColor()
//        contentView.textAlignment = .Center
//        contentView.font = FONT_MEDIUM22
//        contentView.text = "Welcome To PwC Knowledge sharing"
//        shimmerView.contentView = contentView
//        shimmerView.shimmering = true
//        shimmerView.shimmeringSpeed = 100.0
//        animatedItem.textColor = UIColor.redColor()
        // Do any additional setup after loading the view.
    }

    override func configureTableView() {
        tableView.estimatedRowHeight = 100
        tableView.registerNibWithCell(HomeItemCell)
        tableView.registerNibWithCell(PostSketchCell)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func presentToRoundTableViewController() {
        let addVC = AddViewController()
        let nav = UINavigationController(rootViewController: addVC)
        navigationController?.presentViewController(nav, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HomeViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  section == 0 ? 1 : 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueCell(PostSketchCell)
            cell.isHomePage = true
            return cell
        }
        let cell = tableView.dequeueCell(HomeItemCell)
        cell.name.text = nameSections[indexPath.row]
        let number = indexPath.row + 4
        cell.avator.image = UIImage(named: "Oval 46 Copy " + String(number))
        cell.screen.image = UIImage(named: "webinar_img" + String(indexPath.row % 2))

        cell.callBack = {
            [weak self] in
            let bundle = NSBundle.mainBundle().pathForResource("video", ofType: "mp4")
            let url = NSURL(fileURLWithPath: bundle!)
            let mobileplayer = MobilePlayerViewController(contentURL: url)
            mobileplayer.activityItems = [url]
            mobileplayer.title = "Sample"
            self?.presentMoviePlayerViewControllerAnimated(mobileplayer)
            
        }
        return cell
    }
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Round Table" : "For you"
    }
}


extension HomeViewController: UINavigationControllerDelegate {
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == UINavigationControllerOperation.Push {
            return DissolveTransition()
        } else {
            return nil
        }
    }
}


extension HomeViewController: UITabBarControllerDelegate {
    
    func tabBarController(tabBarController: UITabBarController, animationControllerForTransitionFromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DissolveTransition()
    }
}