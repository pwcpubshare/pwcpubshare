//
//  UserViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/15.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit
class UserViewController: UIViewController {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var avatorIcon: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

//        navigationController?.navigationBar.shadowImage = UIImage()
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
//        navigationController?.navigationBar.barTintColor = CustomRedColor
//        navigationItem.title = "Profile"
        collectionView.registerNibWithCell(ImageCollectionCell)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(presentARViewController))
//        avatorIcon.userInteractionEnabled = true
//        avatorIcon.addGestureRecognizer(tapGesture)
        bgImageView.userInteractionEnabled = true
        bgImageView.image = UIImage(named: "Profile")
//        bgImageView.hidden = true
        bgImageView.addGestureRecognizer(tapGesture)
        let view = UIApplication.sharedApplication().valueForKey("statusBar") as? UIView
        view?.backgroundColor = CustomRedColor
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBarHidden = true
    }
    
    func presentARViewController() {
      let vc = ViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserViewController: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(ImageCollectionCell.self, indexPath: indexPath)
        cell.imageName = "landscape" + String(indexPath.item)
        return cell
    }
    
}

extension UserViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 5
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = (UIScreen.mainScreen().bounds.width - 20) / 3
        return CGSize(width: width, height: width)
    }
}
