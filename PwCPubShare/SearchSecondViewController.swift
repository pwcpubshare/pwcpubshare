//
//  SearchSecondViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/27.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit
let descriptions = ["Skills: Objective-c, Swift", "Skills: JavaScript, CSS, Hybrid", "Skills: C#, Java, JavaScript"]
class SearchSecondViewController: UIViewController {
    var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: Screen_Width, height: 40))
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
        navigationItem.titleView = searchBar
        configureTableView()
    }

    override func configureTableView() {
        let tablView = UITableView(frame: view.bounds, style: .Plain)
        tablView.delegate = self
        tablView.dataSource = self
        tablView.estimatedRowHeight = 44
        tablView.separatorStyle = .None
        self.tableView = tablView
        tablView.registerNibWithCell(PostDiscussionCell)
        view.addSubview(tablView)
        
    }
    
    
    func addBackgroundImage() {
        let  imageView = UIImageView(frame: view.frame)
        imageView.backgroundColor = UIColor.whiteColor()
        imageView.image = UIImage(named: "Search_result")
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(play))
        imageView.userInteractionEnabled = true
//        imageView.addGestureRecognizer(tapGesture)
        view.addSubview(imageView)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchSecondViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  section == 0 ? 3 : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(PostDiscussionCell)
        let number = indexPath.row + 4
        cell.name.text = nameSections[indexPath.row]
        cell.jzImageView.image = UIImage(named: "Oval 46 Copy " + String(number))
        cell.descriptionLabel.text = descriptions[indexPath.row]
        cell.name.text = nameSections[indexPath.row]
        
        return cell
    }
    
}

extension SearchSecondViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Related People" : "Related Topics"
    }
}

extension SearchSecondViewController: UISearchBarDelegate {

    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
