//
//  PostDiscussionCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/22.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class PostDiscussionCell: UITableViewCell {

    @IBOutlet weak var jzImageView: JZImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
