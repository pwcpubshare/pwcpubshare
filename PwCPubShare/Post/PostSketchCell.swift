//
//  PostSketchCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 17/1/10.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class PostSketchCell: UITableViewCell {

    @IBOutlet weak var height: NSLayoutConstraint!
    var isHomePage = false {
        didSet {
            height.constant = isHomePage ? 60 : 40
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.registerNibWithCell(ImageCollectionCell)
        collectionView.registerNibWithCell(SquareCell)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(indexPath: NSIndexPath) -> UICollectionViewCell{
        if isHomePage {
            let cell = collectionView.dequeueCell(SquareCell.self, indexPath: indexPath)
            if indexPath.item == 0 {
                cell.type = "Virtual Reality"
                cell.typeLabel.backgroundColor = UIColor(hexValue: 0xffb600)
            } else {
                cell.type = "Smart Phone"
                cell.typeLabel.backgroundColor = UIColor(hexValue: 0xdddddd)
            }
            return cell
        } else {
            let imageCell = collectionView.dequeueCell(ImageCollectionCell.self, indexPath: indexPath)
            let name = "Oval 46 Copy " + String(indexPath.item + 4)
            imageCell.imageName = name
            return imageCell
        }
    }
    
}


extension PostSketchCell: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isHomePage ? 2 : 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return configureCell(indexPath)
    }
    
}

extension PostSketchCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return  CGSize(width: height.constant, height: height.constant)
    }
}