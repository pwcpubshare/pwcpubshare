//
//  SearchViewController.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 2017/2/9.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        navigationController?.navigationBarHidden = true
        let view = UIApplication.sharedApplication().valueForKey("statusBar") as? UIView
        view?.backgroundColor = CustomRedColor

        // Do any additional setup after loading the view.
        configureTableView()
    }
    
    override func configureTableView() {
        tableView.registerNibWithCell(PostDiscussionCell)
        tableView.registerNibWithCell(RoundTableTopicCell)

        tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        
        let secondVC = SearchSecondViewController()
        secondVC.modalTransitionStyle = .CrossDissolve
        secondVC.modalPresentationStyle = .OverCurrentContext
        let nav = UINavigationController(rootViewController: secondVC)
        navigationController?.presentViewController(nav, animated: true, completion: nil)
        return false
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            searchBar.resignFirstResponder()
        }
        return true
    }
    
}

extension SearchViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  section == 0 ? 3 : 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueCell(PostDiscussionCell)
            let number = indexPath.row + 4
            cell.name.text = nameSections[indexPath.row]
            cell.jzImageView.image = UIImage(named: "Oval 46 Copy " + String(number))
            
            cell.name.text = nameSections[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueCell(RoundTableTopicCell)
            cell.title = titleSection[indexPath.row]
            return cell
        }
    }
    
}

extension SearchViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Recent Search" : "For you"
    }
}
