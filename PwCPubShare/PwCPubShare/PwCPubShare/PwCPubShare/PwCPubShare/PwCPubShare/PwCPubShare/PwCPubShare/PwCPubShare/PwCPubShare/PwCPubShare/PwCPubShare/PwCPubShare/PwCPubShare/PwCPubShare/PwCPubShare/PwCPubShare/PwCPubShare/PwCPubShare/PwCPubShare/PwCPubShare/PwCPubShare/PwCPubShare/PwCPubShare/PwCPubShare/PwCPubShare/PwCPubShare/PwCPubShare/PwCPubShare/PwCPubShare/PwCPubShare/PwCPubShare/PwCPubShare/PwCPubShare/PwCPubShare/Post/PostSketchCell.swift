//
//  PostSketchCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 17/1/10.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class PostSketchCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.registerNibWithCell(ImageCollectionCell)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension PostSketchCell: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let imageCell = collectionView.dequeueCell(ImageCollectionCell.self, indexPath: indexPath)
        return imageCell
    }
    
}

extension PostSketchCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
     
        return CGSize(width: 40, height: 40)
    }
}