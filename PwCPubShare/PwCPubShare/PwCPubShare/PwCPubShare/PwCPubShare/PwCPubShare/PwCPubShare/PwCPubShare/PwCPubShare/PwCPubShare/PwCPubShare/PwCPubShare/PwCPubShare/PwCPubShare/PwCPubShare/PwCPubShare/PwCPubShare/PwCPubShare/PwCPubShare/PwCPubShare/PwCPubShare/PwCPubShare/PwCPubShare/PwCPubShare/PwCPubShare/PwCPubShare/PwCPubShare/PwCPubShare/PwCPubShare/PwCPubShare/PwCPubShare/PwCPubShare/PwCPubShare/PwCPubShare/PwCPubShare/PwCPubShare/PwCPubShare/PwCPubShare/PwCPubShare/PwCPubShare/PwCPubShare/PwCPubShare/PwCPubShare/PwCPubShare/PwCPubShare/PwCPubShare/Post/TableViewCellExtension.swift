//
//  TableViewCellExtension.swift
//  REX
//
//  Created by appledev103 on 11/6/15.
//  Copyright © 2015 PwC Inc. All rights reserved.
//

import UIKit

extension UITableView {
    
    func dequeueCell<T: UITableViewCell>(cell: T.Type) -> T {
        return dequeueReusableCellWithIdentifier(T.className) as! T
    }
    
    func registerNibWithCell<T: UITableViewCell>(cell: T.Type) {
        self.registerNib(UINib(nibName: T.className, bundle: nil), forCellReuseIdentifier: T.className)
    }
}

extension UITableViewCell {
    static var className: String{
        return self.description().componentsSeparatedByString(".").last!
    }
}

extension UICollectionViewCell {
    static var className: String{
        return self.description().componentsSeparatedByString(".").last!
    }
}

extension UICollectionView {
    
    func registerNibWithCell<T: UICollectionViewCell>(cell: T.Type) {
        self.registerNib(UINib(nibName: T.className, bundle: nil), forCellWithReuseIdentifier: T.className)
    }
    
    func dequeueCell<T: UICollectionViewCell>(cell: T.Type, indexPath: NSIndexPath) -> T {
        return dequeueReusableCellWithReuseIdentifier(T.className, forIndexPath: indexPath) as! T
    }

}

extension UIViewController {
    
    func cancelButtonAction() {
//        if let vc = self.navigationItem.leftBarButtonItem?.target as? UIViewController {
            navigationController?.dismissViewControllerAnimated(true, completion: nil)
//        }
    }
    
}