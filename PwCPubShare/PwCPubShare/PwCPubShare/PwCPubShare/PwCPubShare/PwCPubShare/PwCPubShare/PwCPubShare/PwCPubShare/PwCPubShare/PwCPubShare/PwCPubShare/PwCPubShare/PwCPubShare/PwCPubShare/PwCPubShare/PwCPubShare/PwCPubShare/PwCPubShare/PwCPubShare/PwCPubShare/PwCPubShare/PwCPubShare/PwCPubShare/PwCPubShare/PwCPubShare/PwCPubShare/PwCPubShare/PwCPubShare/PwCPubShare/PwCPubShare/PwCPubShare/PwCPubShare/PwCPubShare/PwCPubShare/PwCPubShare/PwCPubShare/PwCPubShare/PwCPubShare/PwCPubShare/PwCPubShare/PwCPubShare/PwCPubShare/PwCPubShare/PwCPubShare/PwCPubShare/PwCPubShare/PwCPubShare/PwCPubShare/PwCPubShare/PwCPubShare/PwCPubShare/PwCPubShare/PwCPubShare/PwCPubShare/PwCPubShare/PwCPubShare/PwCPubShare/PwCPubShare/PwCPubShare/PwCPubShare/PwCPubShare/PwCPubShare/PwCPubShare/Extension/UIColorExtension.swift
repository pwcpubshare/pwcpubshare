//
//  UIColorExtension.swift
//  REX
//
//  Created by Aaron Hu on 8/22/15.
//  Copyright (c) 2015 PwC Inc. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int, opacity: CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: opacity)
    }
    
    convenience init(hexValue:Int, opacity: CGFloat = 1.0) {
        self.init(red:(hexValue >> 16) & 0xff, green:(hexValue >> 8) & 0xff, blue:hexValue & 0xff, opacity: opacity)
    }
}

extension String {
    var length: Int{
        get {
            return characters.count
        }
    }
}