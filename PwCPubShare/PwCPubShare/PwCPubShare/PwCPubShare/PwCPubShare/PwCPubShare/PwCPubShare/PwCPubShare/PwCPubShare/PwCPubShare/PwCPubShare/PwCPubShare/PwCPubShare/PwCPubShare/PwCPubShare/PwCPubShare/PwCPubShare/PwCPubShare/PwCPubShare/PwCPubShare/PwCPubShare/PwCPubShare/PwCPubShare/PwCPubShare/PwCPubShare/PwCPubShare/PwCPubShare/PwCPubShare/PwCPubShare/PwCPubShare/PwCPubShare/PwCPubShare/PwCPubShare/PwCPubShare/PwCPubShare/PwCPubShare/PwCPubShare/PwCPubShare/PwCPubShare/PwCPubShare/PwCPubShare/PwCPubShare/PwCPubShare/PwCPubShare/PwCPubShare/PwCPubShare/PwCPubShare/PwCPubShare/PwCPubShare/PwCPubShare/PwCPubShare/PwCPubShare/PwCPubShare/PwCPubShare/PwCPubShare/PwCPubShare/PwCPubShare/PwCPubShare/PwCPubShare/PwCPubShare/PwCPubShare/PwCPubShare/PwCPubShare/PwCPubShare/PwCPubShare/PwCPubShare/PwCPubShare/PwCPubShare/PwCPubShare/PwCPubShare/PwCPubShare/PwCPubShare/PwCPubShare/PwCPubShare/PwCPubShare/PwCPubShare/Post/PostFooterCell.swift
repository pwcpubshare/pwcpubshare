//
//  PostFooterCell.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 17/1/10.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class PostFooterCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    var title: String = "" {
        didSet {
            label.text = title
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
