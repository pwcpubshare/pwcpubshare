//
//  Dissolve.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 17/1/10.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit
let G_PI = CGFloat(M_PI)
let Screen_Width = UIScreen.mainScreen().bounds.width
let Screen_Height = UIScreen.mainScreen().bounds.height
class DissolveTransition: NSObject {
    private var transitionContext: UIViewControllerContextTransitioning?
    
}

extension DissolveTransition: UIViewControllerAnimatedTransitioning {
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.7
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext
        let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        print(toVC?.tabBarItem.accessibilityFrame)
        let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        print(fromVC?.tabBarItem.accessibilityFrame)
        transitionContext.containerView()?.addSubview((toVC!.view)!)
        let x = CGFloat(fromVC?.tabBarController?.selectedIndex ?? 0) * Screen_Width / 5.0
        let rect = CGRect(x: x, y: Screen_Height - 49 * 0.5, width: Screen_Width / 5, height: 49)
        print(rect)
        let maskStartPath = UIBezierPath(ovalInRect: rect)
        
        let maskFinalPath =  UIBezierPath(arcCenter: toVC!.view.center, radius: 1000, startAngle: 0, endAngle: G_PI * 2, clockwise: true)
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskFinalPath.CGPath
        let maskLayerAnimation = CABasicAnimation(keyPath: "path")
        maskLayerAnimation.fromValue = maskStartPath.CGPath
        maskLayerAnimation.toValue  = maskFinalPath.CGPath
        maskLayerAnimation.duration = transitionDuration(transitionContext)
        maskLayerAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        maskLayerAnimation.delegate = self
        toVC!.view.layer.mask = maskLayer
        
        maskLayer.addAnimation(maskLayerAnimation, forKey: "path")
        
    }
}

extension DissolveTransition {
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if let transitionContext = self.transitionContext{
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
            transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.view.layer.mask = nil
            transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)?.view.layer.mask = nil
        }
    }
}