//
//  File.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 17/1/10.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import Foundation

public func isValidEmail(emailString: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluateWithObject(emailString)
}

let Nav_Title_Font = UIFont(name: "ITCCharterCom-BlackItalic", size: 12)
let CustomRedColor = UIColor(hexValue: 0xe0301e)
let CustomGrayColor = UIColor(hexValue: 0x666666)