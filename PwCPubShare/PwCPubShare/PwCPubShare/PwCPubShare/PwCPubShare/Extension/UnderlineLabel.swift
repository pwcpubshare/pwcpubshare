//
//  UnderlineLabe.swift
//  PwCPubShare
//
//  Created by Jun Zhang on 17/1/18.
//  Copyright © 2017年 PwC Inc. All rights reserved.
//

import UIKit

class UnderlineLabel
: UILabel {

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawTextInRect(rect: CGRect) {
        
    }
    override func drawRect(rect: CGRect) {
        // Drawing code
        let c = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(c, rect.width)
        CGContextSetLineCap(c, .Round)
        
        CGContextSetTextDrawingMode(c, .Stroke)
    }

}


class REXCustomSearchBar: UISearchBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customizeSearchBarStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customizeSearchBarStyle()
    }
    
     func customizeSearchBarStyle() {
        
        self.clipsToBounds = true
        backgroundColor = UIColor.whiteColor()
        
        layer.cornerRadius = 4
        self.tintColor = CustomRedColor
        let searchField = self.valueForKey( "_searchField")
        searchField!.setValue(FONT_NORMAL14, forKeyPath: "_placeholderLabel.font")
        searchField!.setValue(CustomGrayColor, forKeyPath: "_placeholderLabel.textColor")
        let searchBarBackground = subviews.first?.subviews.first
        searchBarBackground?.layer.cornerRadius = 4
        let shadowView = subviews.first?.subviews.last?.subviews.first
        shadowView?.hidden = true
    }
    
}