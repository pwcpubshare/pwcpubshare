//
//  LoginViewController.swift
//
//
//  Copyright (c) 2015 PwC Inc. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController{
    
    
//    @IBOutlet weak var avatorView : UIView!
    
    @IBOutlet weak var emailField : UITextField!
    @IBOutlet weak var passwordField : UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailField.spellCheckingType = UITextSpellCheckingType.No

        //        let attributes = [
        //            NSFontAttributeName : FONT_NORMAL16!]
        //        emailField.attributedPlaceholder = NSAttributedString(string: "Email Address", attributes:attributes)
        //        passwordField.attributedPlaceholder = NSAttributedString(string: "Password", attributes:attributes)
        //        self.emailField.text = "l@mailinator.com"
        //        self.passwordField.text = "5525loveJJ"
        //        self.emailField.text = "mvpzhangjun124@163.com"
        //        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "rextest7@sina.com"
        //        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "testing0110@sina.com"
        //        self.passwordField.text = "Qazwsx12"
        //
        //        self.emailField.text = "l@mailinator.com"
        //        self.passwordField.text = "5255loveJJ"
        //        self.emailField.text = "mvpzhangjun124@163.com"
        //        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "l@mailinator.com"
        //        self.passwordField.text = "5255loveJJ"
        //        //        self.emailField.text = "wanchen2mp@163.com"
        //        //        self.passwordField.text = "Wan@163.com"
        //        self.emailField.text = "attesting003@sina.com"
        //        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "mvpzhangjun124@163.com"
        //        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "rextest1@sina.com"
        //        self.passwordField.text = "Pwc_1234"
        //        //        self.emailField.text = "mvpzhangjun124@163.com"
        //        //        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "attesting003@sina.com"
        //        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "elisa_zhang@126.com"
        //        self.passwordField.text = "Pwcwelcome2"
        //        self.emailField.text = "testing0112@sina.com"
        //        self.passwordField.text = "Qazwsx12"
        //
//                        self.emailField.text = "jenny001@grr.la"
//                        self.passwordField.text = "Pwcwelcome1"
        //        //        self.emailField.text = "attesting001@sina.com"
        //        //        self.passwordField.text = "Pwcwelcome1
        //        self.emailField.text = "testing0111@sina.com"
        //        self.passwordField.text = "Qazwsx12"
        //        self.emailField.text = "mvpzhangjun124@163.com"
        //        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "testing0111@sina.com"
        //        self.passwordField.text = "Qazwsx12"
        //
        //                self.emailField.text = "attesting010@sina.com"
        //                self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "mvpzhangjun124@163.com"
        //        self.passwordField.text = "Pwcwelcome1"

//                                self.emailField.text = "michael_fang_2@grr.la"
//                                self.passwordField.text = "Pwcwelcome1" //
        ////
//                        self.emailField.text = "taixiu.zhang@gmail.com"
//                        self.passwordField.text = "123456Qqw" //
        passwordField.secureTextEntry = true
        self.emailField.text = "jun.zhang@pwc.com"
        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "taixiu001@grr.la"
        //        self.passwordField.text = "Pwcwelcome1"
        //
        //                self.emailField.text = "attesting005@sina.com"
        //                        self.passwordField.text = "Pwcwelcome1"
        //        self.emailField.text = "rextest_3749@sina.com"
        //                self.emailField.text = "songweidong185@126.com"
        //                self.passwordField.text = "Pwcwelcome1"
        //                self.emailField.text = "fxu1024001@126.com"
        //                self.passwordField.text = "Xu123456"

//        self.emailField.text = "taixiu001@grr.la"
//        self.passwordField.text = "Pwcwelcome1"
//
//        self.emailField.text = "delete001@delete.com"
//        self.passwordField.text = "Pwcwelcome1"
//
//        self.emailField.text = "paladin001@grr.la"
//        self.passwordField.text = "Pwcwelcome1"
//
//        self.emailField.text = "sylar001@grr.la"
//        self.passwordField.text = "Pwcwelcome1"
    }
    
    
    func appUpdateAvailable(storeInfoURL: String) -> Bool
    {
        var upgradeAvailable = false
        
        // Get the main bundle of the app so that we can determine the app's version number
        let bundle = NSBundle.mainBundle()
        if let infoDictionary = bundle.infoDictionary {
            // The URL for this app on the iTunes store uses the Apple ID for the  This never changes, so it is a constant
            let urlOnAppStore = NSURL(string: storeInfoURL)
            if let dataInJSON = NSData(contentsOfURL: urlOnAppStore!) {
                // Try to deserialize the JSON that we got
                if let lookupResults = try? NSJSONSerialization.JSONObjectWithData(dataInJSON, options: NSJSONReadingOptions()) {
                    // Determine how many results we got. There should be exactly one, but will be zero if the URL was wrong
                    if let resultCount = lookupResults["resultCount"] as? Int {
                        if resultCount == 1 {
                            // Get the version number of the version in the App Store
                            if let appStoreVersion = lookupResults["results"]!![0]["version"] as? String {
                                // Get the version number of the current version
                                if let currentVersion = infoDictionary["CFBundleShortVersionString"] as? String {
                                    // Check if they are the same. If not, an upgrade is available.
                                    
                                    let currentVersion1 = currentVersion.stringByReplacingOccurrencesOfString(".", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                                    
                                    
                                    let appStoreVersion1 = appStoreVersion.stringByReplacingOccurrencesOfString(".", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                                    
                                    
                                    let currentVersion2 = Int(currentVersion1)
                                    let appStoreVersion2 = Int(appStoreVersion1)
                                    
                                    
                                    if currentVersion2 < appStoreVersion2 {
                                        upgradeAvailable = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return upgradeAvailable
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Action Methods
    
    @IBAction func signIn(sender: AnyObject) {
        self.view.endEditing(true)
        let emailAddress = self.emailField.text
        self.emailField.text = emailAddress
        let param = NSMutableDictionary()
        param.setValue("password", forKey: "grant_type")
        param.setValue(self.emailField.text, forKey: "username")
        param.setValue(self.passwordField.text, forKey: "password")
        
        
        let topicGuideViewController = TopicGuideViewController()
        presentViewController(topicGuideViewController, animated: true, completion: nil)
        
        if self.emailField.text!.length == 0 {
//            REXUtilities.showAlert(REX_STRING_SIGN_ERROR, message: REX_STRING__EMAIL_ADDRESS, inViewController: self)
            return
        } else if isValidEmail(self.emailField.text!) {//So EM will not able to login by guid
//            REXUtilities.showAlert(REX_STRING_SIGN_ERROR, message: REX_STRING__EMAIL_ADDRESS_INVALID, inViewController: self)
            return
        } else if self.passwordField.text!.length == 0 {
//            REXUtilities.showAlert(REX_STRING_SIGN_ERROR, message: REX_STRING_PASSWORD_REQUIRED, inViewController: self)
            return
        }
        
    }
    
    @IBAction func redirectToSignUp(sender: AnyObject) {
//        let signUpVC = REXSignUpViewController(nibName: "REXSignUpViewController", bundle: nil)
//        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    //MARK: UITextFieldDelegate
    
    @IBAction func forgotYourPassword(sender: AnyObject) {
        
//        let forgotPasswordVC = REXForgotPasswordController(nibName: "REXForgotPasswordController", bundle: nil)
//        forgotPasswordVC.providesPresentationContextTransitionStyle = true;
//        forgotPasswordVC.definesPresentationContext = true;
//        forgotPasswordVC.modalPresentationStyle = UIModalPresentationStyle.FullScreen;
//        let forgotPasswordNav = REXBaseNavigationController(rootViewController: forgotPasswordVC)
//        self.presentViewController(forgotPasswordNav, animated: true, completion: nil)
        
    }
    
}


